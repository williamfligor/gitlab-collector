from .base import GitlabCollector

from prometheus_client.core import GaugeMetricFamily


class PipelineCollector(GitlabCollector):
    pipeline_status_map = {
        "running": 0,
        "pending": 1,
        "success": 2,
        "failed": 3,
        "canceled": 4,
        "skipped": 5,
    }

    def __init__(self):
        super().__init__(refresh_interval=60 * 60)

        self.database = {}

        self.register_webhook("pipeline")

    def get_key(self, project, branch):
        return "{}:{}".format(project.path_with_namespace, branch.name)

    def refresh_database(self):
        database = {}

        for proj in self.get_projects():
            if not proj.jobs_enabled:
                continue

            for branch in proj.branches.list(as_list=False):
                pipeline = proj.pipelines.list(ref=branch.name, per_page=1, page=1)

                if len(pipeline) > 0:
                    pipeline = proj.pipelines.get(pipeline[0].id)

                    database[self.get_key(proj, branch)] = [proj, branch, pipeline]

        self.database = database

    def update_database(self, event):
        proj = self.gl.projects.get(event["project"]["id"])
        branch = proj.branches.get(event["object_attributes"]["ref"])
        pipeline = proj.pipelines.list(ref=branch.name, per_page=1, page=1)

        if len(pipeline) > 0:
            pipeline = proj.pipelines.get(pipeline[0].id)

            self.database[self.get_key(proj, branch)] = [proj, branch, pipeline]

    @property
    def metrics(self):
        pipeline_labels = ["project", "ref", "user", "username"]

        c_status = GaugeMetricFamily(
            "gitlab_pipeline_status", "Pipeline status", labels=pipeline_labels
        )
        c_duration = GaugeMetricFamily(
            "gitlab_pipeline_duration", "Pipeline duration", labels=pipeline_labels
        )
        c_created_at = GaugeMetricFamily(
            "gitlab_pipeline_created_at", "Pipeline created_at", labels=pipeline_labels
        )

        for proj, branch, pipeline in self.database.values():
            # Only one result can come out of query

            pipeline_duration = pipeline.duration
            if pipeline_duration is None:
                pipeline_duration = -1

            labels = [
                proj.path_with_namespace,
                pipeline.ref,
                pipeline.user["name"],
                pipeline.user["username"],
            ]

            c_status.add_metric(
                labels=self.sanitize_labels(labels),
                value=self.pipeline_status_map[pipeline.status],
            )
            c_duration.add_metric(
                labels=self.sanitize_labels(labels), value=pipeline_duration
            )
            c_created_at.add_metric(
                labels=self.sanitize_labels(labels),
                value=self.to_timestamp(pipeline.created_at),
            )

        return [c_status, c_duration, c_created_at]

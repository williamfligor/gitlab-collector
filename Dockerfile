from python:3

COPY . /app
WORKDIR /app

RUN pip install pipenv
RUN pipenv install --system --deploy

EXPOSE 9118

CMD ["python", "main.py"]

from .base import GitlabCollector

from prometheus_client.core import GaugeMetricFamily


class PathCollector(GitlabCollector):
    def __init__(self):
        super().__init__(refresh_interval=60 * 60)

        self.group_database = {}
        self.project_database = {}

    def refresh_database(self):
        group_database = {}
        project_database = {}

        for group in self.get_groups():
            key = "{}".format(group.full_path)
            group_database[key] = group

        for proj in self.get_projects():
            key = "{}".format(proj.path_with_namespace)
            project_database[key] = proj

        self.group_database = group_database
        self.project_database = project_database

    def update_database(self, event):
        pass

    @property
    def metrics(self):
        membership_labels = ["path"]

        c_path = GaugeMetricFamily("gitlab_path", "Paths", labels=membership_labels)

        for group in self.group_database.values():
            labels = [group.full_path]

            c_path.add_metric(labels=self.sanitize_labels(labels), value=1)

        for proj in self.project_database.values():
            labels = [proj.path_with_namespace]

            c_path.add_metric(labels=self.sanitize_labels(labels), value=1)

        return [c_path]

import json
import traceback
import pprint
import threading
import datetime
import time
import gitlab
import configparser
import fnmatch
import multiprocessing as mp

from aiohttp import web
from prometheus_client import CollectorRegistry, MetricsHandler
from prometheus_client.exposition import _ThreadingSimpleServer, choose_encoder
from prometheus_client.core import GaugeMetricFamily, REGISTRY

from collector.cache import MethodCache
from collector import (
    GitlabCollector,
    IssueCollector,
    MergeRequestCollector,
    PipelineCollector,
    MembershipCollector,
    PathCollector,
    BranchCollector,
    RunnerCollector,
)


class GitlabMetricsHandler(MetricsHandler):
    @classmethod
    def set_post_handler(cls, handler):
        cls.handler = handler

    def do_POST(self):
        data_length = int(self.headers["Content-Length"])
        post_data = self.rfile.read(data_length)

        payload = json.loads(post_data.decode("utf-8"))

        if hasattr(self, "handler"):
            self.handler(payload)

        self.send_response(200)


class GitlabCollectorManager:
    def __init__(self, registry):
        self.registry = registry

        self.pipeline_collector = PipelineCollector()
        self.runner_collector = RunnerCollector()
        self.membership_collector = MembershipCollector()
        self.issue_collector = IssueCollector()
        self.mr_collector = MergeRequestCollector()
        self.path_collector = PathCollector()
        self.branch_collector = BranchCollector()

    def collect(self):
        metrics = []

        metrics += self.membership_collector.collect()
        metrics += self.pipeline_collector.collect()
        metrics += self.runner_collector.collect()
        metrics += self.issue_collector.collect()
        metrics += self.mr_collector.collect()
        metrics += self.path_collector.collect()
        metrics += self.branch_collector.collect()

        return metrics

    async def handle_get(self, request):
        encoder, content_type = choose_encoder(request.headers.get("Accept"))
        output = encoder(self.registry)
        return web.Response(body=output, headers={"Content-Type": content_type})

    async def handle_post(self, request):
        js = await request.json()
        GitlabCollector.handle_webhook(js)
        return web.Response(text="Done")


if __name__ == "__main__":
    registery = CollectorRegistry(auto_describe=True)
    collector = GitlabCollectorManager(registery)

    registery.register(collector)

    app = web.Application()
    app.add_routes(
        [
            web.get("/", collector.handle_get),
            web.get("/metrics", collector.handle_get),
            web.post("/", collector.handle_post),
        ]
    )

    web.run_app(app, port=9118)

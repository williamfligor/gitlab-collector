from .base import GitlabCollector

from prometheus_client.core import GaugeMetricFamily


class IssueCollector(GitlabCollector):
    issue_status_map = {"opened": 0, "closed": 1}

    def __init__(self):
        super().__init__(refresh_interval=5 * 60)

        self.issue_state = self.get_config("issues", "state", default=None)
        self.database = {}

        self.register_webhook("issue")

    def get_key(self, project, mr):
        return "{}:{}".format(project.path_with_namespace, mr.id)

    def refresh_database(self):
        database = {}

        for proj in self.get_projects():
            if not proj.issues_enabled:
                continue

            if self.issue_state is not None:
                issues = proj.issues.list(as_list=False, state=self.issue_state)
            else:
                issues = proj.issues.list(as_list=False)

            for issue in issues:
                database[self.get_key(proj, issue)] = [proj, issue]

        self.database = database

    def update_database(self, event):
        proj = self.gl.projects.get(event["project"]["id"])
        issue = proj.issues.get(event["object_attributes"]["iid"])

        self.database[self.get_key(proj, issue)] = [proj, issue]

    @property
    def metrics(self):
        issue_labels = ["project", "id", "title", "assigned_user", "assigned_username"]

        c_status = GaugeMetricFamily(
            "gitlab_issue_state", "Issue state", labels=issue_labels
        )
        c_created_at = GaugeMetricFamily(
            "gitlab_issue_created_at", "Issue created_at", labels=issue_labels
        )

        for proj, issue in self.database.values():
            if self.issue_state is not None:
                if self.issue_state != issue.state:
                    continue

            assigned_user = "None"
            assigned_username = "None"

            if len(issue.assignees) > 0:
                assigned_user = issue.assignees[0]["name"]
                assigned_username = issue.assignees[0]["username"]

            labels = [
                proj.path_with_namespace,
                str(issue.id),
                issue.title,
                assigned_user,
                assigned_username,
            ]

            c_status.add_metric(
                labels=self.sanitize_labels(labels),
                value=self.issue_status_map[issue.state],
            )
            c_created_at.add_metric(
                labels=self.sanitize_labels(labels),
                value=self.to_timestamp(issue.created_at),
            )

        return [c_status, c_created_at]

from .base import GitlabCollector

from prometheus_client.core import GaugeMetricFamily


class BranchCollector(GitlabCollector):
    def __init__(self):
        super().__init__(refresh_interval=60 * 60)

        self.database = {}

    def refresh_database(self):
        database = {}

        for proj in self.projects:
            if proj.default_branch is None:
                continue

            for branch in proj.branches.list(as_list=False):
                key = "{}:{}".format(proj.path_with_namespace, branch.name)

                database[key] = [proj, branch]

        self.database = database

    def update_database(self, event):
        pass

    @property
    def metrics(self):
        pb_labels = ["project", "ref"]

        c_push = GaugeMetricFamily(
            "gitlab_protected_branch_push", "Protected Branch", labels=pb_labels
        )
        c_merge = GaugeMetricFamily(
            "gitlab_protected_branch_merge", "Protected Branch", labels=pb_labels
        )

        for proj, branch in self.database.values():
            push = 0
            merge = 0

            if branch.protected:
                # Default to super high level
                push = 100000
                merge = 100000

                protection = proj.protectedbranches.get(branch.name)

                # find lower access levels
                for push_access in protection.push_access_levels:
                    push = min(push, push_access["access_level"])

                # find lower access levels
                for merge_access in protection.merge_access_levels:
                    merge = min(merge, merge_access["access_level"])

            labels = [proj.path_with_namespace, branch.name]

            c_push.add_metric(labels=self.sanitize_labels(labels), value=push)
            c_merge.add_metric(labels=self.sanitize_labels(labels), value=merge)

        return [c_push, c_merge]

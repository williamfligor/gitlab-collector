import functools
import time


class MethodCache:
    MINUTE = 60
    HOUR = 60 * 60
    DAY = 60 * 60 * 24

    def __init__(self, refresh_interval=60, debug=False):
        self.refresh_interval = refresh_interval
        self.debug = debug

        self.first_call = True
        self.last_refresh = time.time()
        self.last_return_value = None

    def __call__(self, function, *args, **kwargs):
        @functools.wraps(function)
        def wrapper(*args, **kwargs):
            time_since = time.time() - self.last_refresh

            if self.first_call or time_since >= self.refresh_interval:
                if self.debug:
                    print(
                        "MethodCache {}: Cache Expired - Refreshing".format(
                            function.__name__
                        )
                    )

                self.last_return_value = function(*args, **kwargs)

                self.last_refresh = time.time()
                self.first_call = False
            else:
                if self.debug:
                    print(
                        "MethodCache {}: Using cached value".format(function.__name__)
                    )

            return self.last_return_value

        return wrapper

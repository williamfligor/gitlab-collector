#!/usr/bin/python

import time
import asyncio
import configparser
import datetime
import json
import fnmatch
import gitlab

from prometheus_client import start_http_server, CollectorRegistry
from prometheus_client.core import GaugeMetricFamily, REGISTRY

from .cache import MethodCache

loop = asyncio.get_event_loop()


class GitlabCollector(object):
    init_class = False
    gl = None
    webhook_registry = {}

    projects = []
    groups = []

    def __init__(self, refresh_interval=5):
        self.refresh_interval = refresh_interval
        self.last_refresh = 0

        self.refresh_running = False

        self.cached_metrics = []

        if not self.init_class:
            GitlabCollector.class_init()

    def refresh_database(self):
        raise NotImplementedError("Must be implemented")

    def update_database(self, event):
        raise NotImplementedError("Must be implemented")

    @property
    def metrics(self):
        raise NotImplementedError("Must be implemented")

    def refresh(self):
        time_since = time.time() - self.last_refresh

        if self.last_refresh == 0 or time_since >= self.refresh_interval:
            self.last_refresh = time.time()

            print("Refreshing {}".format(self.__class__.__name__))
            self.queue.put_nowait((self._refresh_database, []))

    def update(self, event):
        print("Updating {}".format(self.__class__.__name__))
        self.queue.put_nowait((self._update_database, [event]))

    def _refresh_database(self):
        self.refresh_database()

        self.cached_metrics = self.metrics

    def _update_database(self, event):
        self.update_database(event)

        self.cached_metrics = self.metrics

    def collect(self):
        self.refresh()

        return self.cached_metrics

    def _update_database(self, event):
        self.update_database(event)


    def register_webhook(self, object_kind):
        if object_kind not in self.webhook_registry:
            self.webhook_registry[object_kind] = []

        self.webhook_registry[object_kind].append(self)

    @classmethod
    def handle_webhook(cls, evt):
        kind = evt["object_kind"]

        if kind in cls.webhook_registry:
            handler_objs = cls.webhook_registry[kind]
            for handler_obj in handler_objs:
                handler_obj.update(evt)
        else:
            print('No handler for webhook object_kind "{}"'.format(kind))

    @classmethod
    @MethodCache(refresh_interval=MethodCache.HOUR)
    def get_groups(cls):
        groups = cls.gl.groups.list(as_list=False)
        for group in groups:
            include_group = True

            if len(cls.filters) > 0:
                include_group = False

                for group_filter in cls.filters:
                    if fnmatch.fnmatch(group.full_path, group_filter):
                        include_group = True
                        break

            if include_group:
                cls.groups.append(group)

        return cls.groups

    @classmethod
    @MethodCache(refresh_interval=MethodCache.HOUR)
    def get_projects(cls):
        projects = cls.gl.projects.list(membership=True, as_list=False)
        for project in projects:
            include_project = True

            if len(cls.filters) > 0:
                include_project = False

                for project_filter in cls.filters:
                    if fnmatch.fnmatch(project.path_with_namespace, project_filter):
                        include_project = True
                        break

            if include_project:
                cls.projects.append(project)

        return cls.projects

    def sanitize_labels(self, labels):
        return list(map(lambda label: str(label), labels))

    def to_timestamp(self, date):
        date = date.replace("Z", "+00:00")
        date = datetime.datetime.fromisoformat(date)
        date = date.replace(tzinfo=datetime.timezone.utc).timestamp() * 1000

        return date

    @classmethod
    def class_init(cls):
        cls.init_class = True

        cls.gl = gitlab.Gitlab.from_config("gitlab", ["config.cfg"])

        cls.config = configparser.ConfigParser()
        cls.config.read("config.cfg")

        cls.filters = []

        cls.mr_state = "all"
        cls.issue_state = "all"

        # Parse out filters config
        filters = cls.get_config("filter", "filter", "[]")
        filter_json = json.loads(filters)
        for filt in filter_json:
            cls.filters.append(filt)

        # Parse out runner config
        runners = cls.get_config("runners", "runners", [])
        for key in runners:
            cls.runner_ids.append(runners[key])

        cls.queue = asyncio.Queue()
        loop.create_task(cls.worker(cls.queue))

    @classmethod
    async def worker(self, queue):
        while True:
            work, args = await queue.get()
            await loop.run_in_executor(None, work, *args)
            queue.task_done()

    @classmethod
    def get_config(cls, key1, key2, default=None):
        if key1 not in cls.config:
            return default

        key1_value = cls.config[key1]

        if key2 not in key1_value:
            return default

        return key1_value[key2]

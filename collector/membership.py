from .base import GitlabCollector

from prometheus_client.core import GaugeMetricFamily


class MembershipCollector(GitlabCollector):
    def __init__(self):
        super().__init__(refresh_interval=60 * 60)

        self.group_database = {}
        self.project_database = {}

    def refresh_database(self):
        group_database = {}
        project_database = {}

        for group in self.get_groups():
            for member in group.members.list(as_list=False):
                key = "{}:{}".format(group.full_path, member.username)
                group_database[key] = [group, member]

        for proj in self.get_projects():
            for member in proj.members.list(as_list=False):
                key = "{}:{}".format(proj.path_with_namespace, member.username)
                project_database[key] = [proj, member]

        self.group_database = group_database
        self.project_database = project_database

    def update_database(self, event):
        pass

    @property
    def metrics(self):
        membership_labels = ["path", "user", "username"]

        c_membership = GaugeMetricFamily(
            "gitlab_membership", "Membership", labels=membership_labels
        )

        for group, member in self.group_database.values():
            labels = [group.full_path, member.name, member.username]

            c_membership.add_metric(
                labels=self.sanitize_labels(labels), value=member.access_level
            )

        for proj, member in self.project_database.values():
            labels = [proj.path_with_namespace, member.name, member.username]

            c_membership.add_metric(
                labels=self.sanitize_labels(labels), value=member.access_level
            )

        return [c_membership]

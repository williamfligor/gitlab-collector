from .base import GitlabCollector

from prometheus_client.core import GaugeMetricFamily


class MergeRequestCollector(GitlabCollector):
    mr_status_map = {"opened": 0, "closed": 1, "locked": 2, "merged": 3}

    def __init__(self):
        super().__init__(refresh_interval=60 * 60)

        self.mr_state = self.get_config("merge_requests", "state", default=None)
        self.database = {}

        self.register_webhook("merge_request")

    def get_key(self, project, mr):
        return "{}:{}".format(project.path_with_namespace, mr.id)

    def refresh_database(self):
        database = {}

        for proj in self.get_projects():
            if not proj.merge_requests_enabled:
                continue

            if self.mr_state is not None:
                mrs = proj.mergerequests.list(as_list=False, state=self.mr_state)
            else:
                mrs = proj.mergerequests.list(as_list=False)

            for mr in mrs:
                database[self.get_key(proj, mr)] = [proj, mr]

        self.database = database

    def update_database(self, event):
        proj = self.gl.projects.get(event["project"]["id"])
        mr = proj.mergerequests.get(event["object_attributes"]["iid"])

        self.database[self.get_key(proj, mr)] = [proj, mr]

    @property
    def metrics(self):
        mr_labels = [
            "project",
            "id",
            "wip",
            "title",
            "assigned_user",
            "assigned_username",
        ]

        c_status = GaugeMetricFamily(
            "gitlab_merge_request_state", "Merge request state", labels=mr_labels
        )
        c_created_at = GaugeMetricFamily(
            "gitlab_merge_request_created_at",
            "Merge request created_at",
            labels=mr_labels,
        )
        c_updated_at = GaugeMetricFamily(
            "gitlab_merge_request_updated_at",
            "Merge request updated_at",
            labels=mr_labels,
        )

        for proj, mr in self.database.values():
            if self.mr_state is not None:
                if self.mr_state != mr.state:
                    continue

            assigned_user = "None"
            assigned_username = "None"

            if mr.assignee is not None:
                assigned_user = mr.assignee["name"]
                assigned_username = mr.assignee["username"]

            labels = [
                proj.path_with_namespace,
                str(mr.id),
                str(mr.work_in_progress),
                str(mr.title),
                str(assigned_user),
                str(assigned_username),
            ]

            c_status.add_metric(
                labels=self.sanitize_labels(labels), value=self.mr_status_map[mr.state]
            )
            c_created_at.add_metric(
                labels=self.sanitize_labels(labels),
                value=self.to_timestamp(mr.created_at),
            )
            c_updated_at.add_metric(
                labels=self.sanitize_labels(labels),
                value=self.to_timestamp(mr.updated_at),
            )

        return [c_status, c_created_at, c_updated_at]

from .base import GitlabCollector

from prometheus_client.core import GaugeMetricFamily


class RunnerCollector(GitlabCollector):
    runner_status_map = {
        "active": 0,
        "online": 1,
        "paused": 2,
        "offline": 3,
        "not_connected": 4,
    }

    def __init__(self):
        super().__init__(refresh_interval=30 * 60)

        self.runner_ids = []

        if "runners" in self.config:
            runners = self.config["runners"]

            for key in runners:
                self.runner_ids.append(runners[key])

    def refresh_database(self):
        pass

    def update_database(self, event):
        pass

    @property
    def metrics(self):
        runner_labels = ["id", "name"]

        c_runner = GaugeMetricFamily(
            "gitlab_runner_status", "Runners", labels=runner_labels
        )

        for runner_id in self.runner_ids:
            runner = self.gl.runners.get(runner_id)

            labels = [runner.id, runner.name]

            c_runner.add_metric(
                labels=self.sanitize_labels(labels),
                value=self.runner_status_map[runner.status],
            )

        return [c_runner]

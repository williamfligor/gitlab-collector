from .runner import RunnerCollector
from .membership import MembershipCollector
from .branch import BranchCollector
from .path import PathCollector
from .pipeline import PipelineCollector
from .issue import IssueCollector
from .merge_request import MergeRequestCollector
from .base import GitlabCollector
